FROM frolvlad/alpine-glibc:alpine-3.8

MAINTAINER gregoire.leroy@3e.eu

# Setup system environment
RUN apk add --no-cache wget ca-certificates bash bash-completion build-base readline readline-dev

# Configure environment
ENV SHELL /bin/bash

# Install conda
ENV CONDA_DIR="/opt/conda"
ENV PATH="$CONDA_DIR/bin:$PATH"

RUN mkdir -p "$CONDA_DIR" && \
    wget "http://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh" -O miniconda.sh && \
    bash miniconda.sh -f -b -p "$CONDA_DIR" && \
    echo "export PATH=$CONDA_DIR/bin:\$PATH" > /etc/profile.d/conda.sh && \
    rm miniconda.sh && \
    \
    conda update --all --yes && \
    conda config --set auto_update_conda False && \
    rm -r "$CONDA_DIR/pkgs/" && \
    \
    mkdir -p "$CONDA_DIR/locks" && \
    chmod 777 "$CONDA_DIR/locks"

# Install python packages
ENV MPLBACKEND=agg

# create a dedicated python env using conda
ADD environment.yml .

RUN conda env create -f environment.yml && \
    echo "source activate $(head -1 environment.yml | cut -d' ' -f2)" > ~/.bashrc && \
    conda clean -a

# add env as default when running container
ENV PATH /opt/conda/envs/$(head -1 environment.yml | cut -d' ' -f2)/bin:$PATH

ENTRYPOINT []

CMD [ "/bin/bash" ]